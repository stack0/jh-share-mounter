#!/bin/bash

###########################################################################################
# CHANGE THESE VALUES
###########################################################################################
MANILA_SHARE_ACCESS_KEY=""
MANILA_SHARE_ACCESS_TO=""

# to get this, you can get this info using cli or horizon openstack share show <uuid>, look in export_locations, under path
MANILA_CEPH_MONITORS=""
MANILA_CEPH_ROOT_PATH=""

# Give the estimated size in gigabytes, e.g. 1TB = 1000 (an estimate is OK)
# It does not need to be precise
MANILA_SHARE_SIZE=1000

# you don't need to change this, but you can if you want
MOUNT_DIR=/home/jovyan/shared

###########################################################################################
# END CHANGE THESE VALUES
###########################################################################################

###
# Only change these values if you know what you're doing
###
CEPH_CSI_DIR="/opt/ceph_csi_driver"
CEPH_CSI_CHART_VERSION="3.6.2"
JH_SHARED_STORAGE_PV_NAME=${MANILA_SHARE_ACCESS_TO}-pv
JH_SHARED_STORAGE_PVC_NAME=${MANILA_SHARE_ACCESS_TO}-pvc
MANILA_SHARE_ACCESS_MODE="ReadWriteMany" # another option could be "ReadOnlyMany"

date
if [ ! -d $CEPH_CSI_DIR ]; then
    echo "Attempting to create $CEPH_CSI_DIR"
    mkdir ${CEPH_CSI_DIR}
    if [ $? != 0 ]; then
        echo "?Could not create directory, ${CEPH_CSI_DIR}; try using sudo first"
        exit 1
    fi
fi

echo "Helm adding repo"
helm repo add ceph-csi https://ceph.github.io/csi-charts
if [ $? -ne 0 ]; then
    echo "?Could not add the csi repo"
    exit 1
fi

echo "Helm updating repo"
helm repo update

helm get all -n ceph-csi-cephfs ceph-csi-cephfs
if [ $? -ne 0 ]; then
    helm  install -n ceph-csi-cephfs --version $CEPH_CSI_CHART_VERSION --create-namespace --set storageClass.create=true,storageClass.reclaimPolicy="Retain" ceph-csi-cephfs ceph-csi/ceph-csi-cephfs 
    if [ $? -ne 0 ]; then
        echo "?The Ceph CSI driver was not found and Could not install it. Exiting..."
        exit 1
    fi
fi
echo "Helm Ceph CSI driver should be detected or installed at this point."

echo "Creating ${CEPH_CSI_DIR}/csi-cephfs-secret.yaml"
cat >${CEPH_CSI_DIR}/csi-cephfs-secret.yaml <<EOL
apiVersion: v1
kind: Secret
metadata:
    name: csi-cephfs-secret
    namespace: ceph-csi-cephfs
data:
    userKey: $(echo -n $MANILA_SHARE_ACCESS_KEY | base64 -w0)
    userID: $(echo -n $MANILA_SHARE_ACCESS_TO | base64 -w0)
EOL

echo "Creating ${CEPH_CSI_DIR}/ceph-csi-config.yaml"
cat >${CEPH_CSI_DIR}/ceph-csi-config.yaml <<EOL
apiVersion: v1
kind: ConfigMap
metadata:
    name: ceph-csi-config
    namespace: ceph-csi-cephfs
data:
    config.json: |-
      [
        {
          "clusterID": ""
        }
      ]
EOL

echo "Creating ${CEPH_CSI_DIR}/ceph_pv.yaml"
cat >${CEPH_CSI_DIR}/ceph_pv.yaml <<EOL
apiVersion: v1
kind: PersistentVolume
metadata:
  name: ${JH_SHARED_STORAGE_PV_NAME}
spec:
  accessModes:
    - ${MANILA_SHARE_ACCESS_MODE}
  capacity:
    storage: ${MANILA_SHARE_SIZE}Gi
  csi:
    driver: cephfs.csi.ceph.com

    # Unique identifier of the volume.
    # Can be anything, but we recommend to set it to the PersistentVolume name.
    volumeHandle: ${JH_SHARED_STORAGE_PV_NAME}

    # Secret name and namespace in both nodeStageSecretRef
    # and nodePublishSecretRef must match the Secret created above.
    nodeStageSecretRef:
      name: csi-cephfs-secret
      namespace: ceph-csi-cephfs
    nodePublishSecretRef:
      name: csi-cephfs-secret
      namespace: ceph-csi-cephfs

    volumeAttributes:
      # The volume attributes below are passed to the cephfs-csi driver.
      # For complete list of available volume parameters please see
      # https://github.com/ceph/ceph-csi/blob/devel/docs/deploy-cephfs.md

      monitors: ${MANILA_CEPH_MONITORS}
      rootPath: ${MANILA_CEPH_ROOT_PATH}
      provisionVolume: "false"
EOL

echo "Creating ${CEPH_CSI_DIR}/ceph_pvc.yaml"
cat >${CEPH_CSI_DIR}/ceph_pvc.yaml <<EOL
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: ${JH_SHARED_STORAGE_PVC_NAME}
spec:
  accessModes:
   - ${MANILA_SHARE_ACCESS_MODE}
  resources:
    requests:
      storage: ${MANILA_SHARE_SIZE}Gi
  volumeName: ${JH_SHARED_STORAGE_PV_NAME} # must match PV name above
  storageClassName: ""
EOL

echo "Applying ${CEPH_CSI_DIR}/csi-cephfs-secret.yaml"
kubectl apply -f ${CEPH_CSI_DIR}/csi-cephfs-secret.yaml
echo "Applying ${CEPH_CSI_DIR}/ceph-csi-config.yaml"
kubectl apply -f ${CEPH_CSI_DIR}/ceph-csi-config.yaml
echo "Applying ${CEPH_CSI_DIR}/ceph_pv.yaml"
kubectl apply -f ${CEPH_CSI_DIR}/ceph_pv.yaml
echo "Applying ${CEPH_CSI_DIR}/ceph_pvc.yaml"
kubectl apply -f ${CEPH_CSI_DIR}/ceph_pvc.yaml

echo "Copying jupyterhub config to a separate file"
cp /opt/jupyterhub/config.yaml /opt/jupyterhub/config-with-share.yaml
if [ $? -ne 0 ]; then
    echo "?Could not create file /opt/jupyterhub/config-with-share.yaml. Try using sudo"
    exit 1
fi

echo "Uncommenting lines in /opt/jupyterhub/config-with-share.yaml"
sed -i '/#  storage:/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#    extraVolumes:/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#    - name: "JH_SHARED_STORAGE_PV_NAME"/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#      persistentVolumeClaim:/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#        claimName: "JH_SHARED_STORAGE_PVC_NAME"/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#    extraVolumeMounts:/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#    - name: "JH_SHARED_STORAGE_PV_NAME"/s/^#//' /opt/jupyterhub/config-with-share.yaml
sed -i '/#      mountPath: JH_SHARED_STORAGE_MOUNT_DIR/s/^#//' /opt/jupyterhub/config-with-share.yaml

echo "Replacing JH_SHARED_STORAGE_PV_NAME in /opt/jupyterhub/config-with-share.yaml"
sed -i 's/JH_SHARED_STORAGE_PV_NAME/'${JH_SHARED_STORAGE_PV_NAME}'/g' /opt/jupyterhub/config-with-share.yaml
echo "Replacing JH_SHARED_STORAGE_PVC_NAME in /opt/jupyterhub/config-with-share.yaml"
sed -i 's/JH_SHARED_STORAGE_PVC_NAME/'${JH_SHARED_STORAGE_PVC_NAME}'/g' /opt/jupyterhub/config-with-share.yaml
echo "Replacing JH_SHARED_STORAGE_MOUNT_DIR in /opt/jupyterhub/config-with-share.yaml"
sed -i 's/JH_SHARED_STORAGE_MOUNT_DIR/'${MOUNT_DIR//\//\\\/}'/g' /opt/jupyterhub/config-with-share.yaml

echo
echo "Attempting to update using new config file /opt/jupyterhub/config-with-share.yaml"
helm upgrade -f /opt/jupyterhub/config-with-share.yaml jupyterhub jupyterhub/jupyterhub

if [ $? -eq 0 ]; then
    echo
    echo "If something catastrophically failed, you can try to restore the original configuration using this command:"
    echo
    echo "      helm upgrade -f /opt/jupyterhub/config.yaml jupyterhub jupyterhub/jupyterhub"
    echo
    echo "SUCCESS! No other action should be needed and the shared folder will be found in ${MOUNT_DIR} in each notebook"
    echo
else
    echo
    echo "FAILURE possibly detected or else a non-zero status was returned"
    echo
    echo "If something catastrophically failed, you can try to restore the original configuration using this command:"
    echo
    echo "      helm upgrade -f /opt/jupyterhub/config.yaml jupyterhub jupyterhub/jupyterhub"
    echo
fi
date

