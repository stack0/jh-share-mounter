# jh-share-mounter

This repo contains a script that, when properly configured, will mount an existing share into a jupyterhub that was launched by CACAO on Jetstream. If there are any questions, please contact Edwin.

## Instructions
1. Login to the master node of the jupyterhub
2. clone the repo into /opt/
3. cd /opt/jh-share-mounter
4. edit the 6 values at the top. note, `MOUNT_DIR` config is optional to change though it is recommended to be at the defaulted location
5. `chmod a+x jhmounter.sh` (if necessary)
6. `./jhmounter.sh`
7. *cross fingers*

## Notes
* Any existing jupyter notebooks may need to be stopped within the jupyterhub admin interface to detect the new configuration
